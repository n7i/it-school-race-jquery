﻿angular.module('geoLocation', [])

.service('geoLocationService', function ($q, $window) {
    var self = this,
        defaultFormatter = function (value) { return value; };

    self.isGeoLocationSupported = function () {
        return $window.navigator.geolocation ? true : false;
    };

    self.GEO_LOCATION_UNSUPPORTED = 'GEO_LOCATION_UNSUPPORTED';
    self.GEO_LOCATION_PERMISSION_DENIED = 'GEO_LOCATION_PERMISSION_DENIED';
    self.GEO_LOCATION_POSITION_NOT_AVAILABLE = 'GEO_LOCATION_POSITION_NOT_AVAILABLE';
    self.GEO_LOCATION_TIMEOUT = 'GEO_LOCATION_POSITION_NOT_AVAILABLE';
    self.GEO_LOCATION_WAITING_FOR_LOCATION = 'GEO_LOCATION_WAITING_FOR_LOCATION';

    self.getLocation = function (formatterFn) {
        var deferred = $q.defer(),
            formatter = typeof formatterFn === 'function' ? formatterFn : defaultFormatter;
        
        if (self.isGeoLocationSupported()) {

            // Simulated location for file: protocol
            if (location.protocol === 'file:') {
                deferred.resolve(
                    formatter(
                      {
                          "timestamp": 1416698602452,
                          "coords": {
                              "speed": null,
                              "heading": null,
                              "altitudeAccuracy": null,
                              "accuracy": 2392, "altitude": null,
                              "longitude": 2.8884657,
                              "latitude": 48.956201799999995
                          }
                      }));
            }
            else {
                deferred.notify(self.GEO_LOCATION_WAITING_FOR_LOCATION);

                $window.navigator.geolocation.getCurrentPosition(function (postion) {
                   deferred.resolve(
                    formatter(postion));
                }, function (error) {

                    var reason = self.GEO_LOCATION_UNSUPPORTED;

                    switch (error.code) {
                        case 1:
                            reason = self.GEO_LOCATION_PERMISSION_DENIED;
                            break;
                        case 2:
                            reason = self.GEO_LOCATION_POSITION_NOT_AVAILABLE;
                            break;
                        case 3:
                            reason = self.GEO_LOCATION_TIMEOUT;
                            break;
                    }

                    deferred.reject(reason);
                });
            }
        }
        else {
            deferred.reject(self.GEO_LOCATION_UNSUPPORTED);
        }

        return deferred.promise;
    };
})

;