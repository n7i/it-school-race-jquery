﻿angular.module('secret', [])

.directive('konami', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            var keys = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];
            var i = 0;
            $(document).keydown(function (e) {
                if (e.keyCode === keys[i++]) {
                    if (i === keys.length) {

                        /* jshint -W059 */
                        $(document).unbind('keydown', arguments.callee);
                        window.open('http://nyanit.com/budgetnow.fr', '_blank');
                    }
                } else {
                    i = 0;
                }
            });
        }
    };
});