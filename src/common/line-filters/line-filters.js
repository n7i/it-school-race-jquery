﻿angular.module('lineFilters', [])

.filter('lineTrunc', function () {
    return function (input, count) {
        if (!input || typeof input !== 'string') {
            return input;
        }

        if (!count) {
            count = 1;
        }
        
        var buffer = '',
            data =  input.split('\n');

        for (var i = 0; i < count && i < data.length; ++i) {
            buffer += data[i];
        }
        
        return buffer;
    };
})

;