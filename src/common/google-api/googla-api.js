﻿angular.module('googleApi', ['geoLocation'])

.factory('googleGeoLocationFormatter', function ($window) {
    return function (gLocation) {
        return new $window.google.maps.LatLng(gLocation.coords.latitude, gLocation.coords.longitude);
    };
})

.service('googleMapService', function ($window, $q, $rootScope, geoLocationService, googleGeoLocationFormatter) {
    var self = this,
        apiConfig = {
            near: {
                radius: 1000, // 1KM in metter
                opennow: true,
                location: null,
                types: [],
                name: '',
                keyword: '',
                minPriceLevel: 0,
                maxPriceLevel: 4
            },
            text: {
                query: '',
                radius: 1000, // 1KM in metter
                opennow: true,
                location: null,
                types: [],
                minPriceLevel: 0,
                maxPriceLevel: 4
            }
        };

    self.findNearDABWithoutMap = function (params) {
        var config = params || {};
        config.types = ['atm'];

        return self.findPlacesByNearWithoutMap(config);
    };

    self.findPlacesByNearWithoutMap = function (params) {
        var uniqId = Date.now().toString(),
            deferred = $q.defer();

        $('body').append('<div style="display:none" id="' + uniqId + '"></div>');
        self.findPlacesbyNear(
                new google.maps.Map($('#' + uniqId)[0]),
                params
            ).then(
                function (results) {
                    deferred.resolve(results);
                    $('body').remove('#' + uniqId);
                },
                function (reason) {
                    deferred.reject(reason);
                    $('body').remove('#' + uniqId);
                });


        return deferred.promise;
    };

    self.findPlacesByTextWithoutMap = function (params) {
        var uniqId = Date.now().toString(),
            deferred = $q.defer();

        $('body').append('<div style="display:none" id="' + uniqId + '"></div>');
        self.findPlacesByText(
                new google.maps.Map($('#' + uniqId)[0]),
                params
            ).then(
                function (results) {
                    deferred.resolve(results);
                    $('body').remove('#' + uniqId);
                },
                function (reason) {
                    deferred.reject(reason);
                    $('body').remove('#' + uniqId);
                });

        return deferred.promise;
    };

    self.findNearDAB = function (map, params) {
        var config = $.extend({}, apiConfig.near, params || {});
        config.types = ['atm'];

        return self.findPlacesbyNear(map, config);
    };

    self.findPlacesbyNear = function (map, params) {

        var placesFinder = new $window.google.maps.places.PlacesService(map),
            query = function (deferred, config) {

                $rootScope.$broadcast('ajaxSend');
                placesFinder.nearbySearch(config, function (result, status) {
                    $rootScope.$broadcast('ajaxComplete');

                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        deferred.resolve(result);
                    }
                    else {
                        deferred.reject(status);
                    }
                });
            },
            config = $.extend({}, apiConfig.near, params || {}),
            deferred = $q.defer();

        if (geoLocationService.isGeoLocationSupported() && !config.location) {
            geoLocationService.getLocation(googleGeoLocationFormatter)
                .then(function (result) {

                    config.location = result;
                    query(deferred, config);
                },
                function (reason) {
                    deferred.reject(reason);
                });
        }
        else {
            query(deferred, config);
        }

        return deferred.promise;
    };

    self.findPlacesByText = function (map, params) {
        var placesFinder = new $window.google.maps.places.PlacesService(map),
            query = function (deferred, config) {

                $rootScope.$broadcast('ajaxSend');
                placesFinder.textSearch(config, function (result, status) {
                    $rootScope.$broadcast('ajaxComplete');

                    if (status == google.maps.places.PlacesServiceStatus.OK) {
                        deferred.resolve(result);
                    }
                    else {
                        deferred.reject(status);
                    }
                });
            },
            config = $.extend({}, apiConfig.near, params || {}),
            deferred = $q.defer();

        if (geoLocationService.isGeoLocationSupported() && !config.location) {
            geoLocationService.getLocation(googleGeoLocationFormatter)
               .then(function (result) {

                   config.location = result;
                   query(deferred, config);
               },
               function (reason) {
                   deferred.reject(reason);
               });
        }
        else {
            query(deferred, config);
        }

        return deferred.promise;
    };

    self.getPlaceDetails = function (map, placeResult) {
        var deferred = $q.defer(),
            service = new $window.google.maps.places.PlacesService(map);

        $rootScope.$broadcast('ajaxSend');
        service.getDetails({ placeId: placeResult.place_id }, function (place, status) {
            $rootScope.$broadcast('ajaxComplete');

            if (status === $window.google.maps.places.PlacesServiceStatus.OK) {
                deferred.resolve(place);
            }
            else {
                deferred.reject(status);
            }
        });

        return deferred.promise;
    };

    self.getDirectionTo = function (destination, travelMode) {
        var deferred = $q.defer(),
            service = new $window.google.maps.DirectionsService();

        geoLocationService.getLocation(googleGeoLocationFormatter)
            .then(function (location) {

                $rootScope.$broadcast('ajaxSend');
                service.route({
                    origin: location,
                    destination: destination,
                    travelMode: travelMode || $window.google.maps.TravelMode.WALKING
                }, function (result, status) {
                    $rootScope.$broadcast('ajaxComplete');

                    if (status == google.maps.DirectionsStatus.OK) {
                        deferred.resolve(result);
                    }
                    else {
                        deferred.reject(status);
                    }
                });
            },
            function (reason) {
                deferred.reject(reason);
            });

        return deferred.promise;
    };
})

.directive('googleMapGeoLocalized', function ($window, $timeout, googleMapService, geoLocationService, googleGeoLocationFormatter) {
    return {
        priority: Number.MAX_VALUE,
        restrict: 'C',
        scope: {
            directionPanel: '@',
            hereMarker: '@',
            marker: '@',
            mapOptions: '@',
            markerClustererOptions: '@',
            enabledMarkerClusterer: '@',
            api: '='
        },
        link: function (scope, element, attrs) {
            scope.config = {
                directionPanel: {
                    enabled: scope.directionPanel || false,
                    height: 250,
                    isOpen: false,
                    directionSet: false
                },
                hereMarker: scope.hereMarker || 'http://cottagesalonspa.com/img/location-icon.png',
                marker: scope.marker || 'https://maps.gstatic.com/mapfiles/ms2/micons/blue.png',
                useMarkerClusterer: scope.enabledMarkerClusterer || false
            };

            var api = {
                getConfig: function() {
                    return scope.config;
                },
                showLocationNotAvailable: function (err) {
                    $(element)
                        .prepend('<div class="map-canvas-geolocation-error">' + err + '</div>')
                        .find('.map-canvas-geolocation-error')
                        .first()
                            .css('vertical-align', 'middle')
                            .css('line-height', $(element).height() + 'px')
                            .hide()
                            .slideDown();
                },
                drawMap: function (map) {
                    geoLocationService.getLocation(googleGeoLocationFormatter)
                        .then(function (result) {
                            $(element)
                                .find('.map-canvas-geolocation-error')
                                .first()
                                .slideUp()
                                .remove();

                            lastLocalizedPosition = result;

                            // Show custom action button
                            api.showCustomUi();

                            // Draw "me" marker
                            api.drawImHereMarker(result, map);

                            // Draw matched places
                            api.drawMarkers(map);

                            // Center on "me"
                            map.setCenter(result);
                        },
                        function (reason) {
                            api.showLocationNotAvailable(reason);
                        },
                        function (notify) {
                            api.showLocationNotAvailable(notify);
                        });
                },
                drawMarkers: function (map) {
                    googleMapService.findNearDAB(map)
                        .then(function (results) {

                            $.each(results, function (index, placeResult) {

                                var markerOptn = {
                                    map: map,
                                    position: placeResult.geometry.location,
                                    title: placeResult.name,
                                    animation: google.maps.Animation.DROP,
                                    icon: scope.config.marker
                                };

                                googleMapService.getPlaceDetails(map, placeResult)
                                    .then(function (result) {
                                        if (result.photos) {
                                            mapOptions.icon = photos[0].getUrl({ 'maxWidth': 35, 'maxHeight': 35 });
                                        }

                                        api.drawMarker(markerOptn, placeResult);
                                    },
                                    function (reason) {
                                        api.drawMarker(markerOptn, placeResult);
                                    });
                            });
                        });
                },
                drawImHereMarker: function (position, map) {
                    if (api.drawedMarker['imheremarker'] !== undefined) {
                        api.drawedMarker['imheremarker'].marker.setMap(null);
                        delete api.drawedMarker['imheremarker'];
                    }

                    var markerOptn = {
                        map: map,
                        position: position,
                        title: 'Vous êtes ici.',
                        icon: scope.config.hereMarker
                    };

                    api.marker.beforeDrawMarker(markerOptn, { place_id: 'imheremarker', types: ['imhere'] });
                    var makerInfo = { marker: new $window.google.maps.Marker(markerOptn), placeResult: { place_id: 'imheremarker', types: ['imhere'] } };

                    $window.google.maps.event.addListener(makerInfo.marker, 'click', function () {
                        var marker = this;
                        api.marker.click(marker, true);
                    });

                    $window.google.maps.event.addListener(makerInfo.marker, 'dblclick', function () {
                        var marker = this;
                        api.marker.dbclick(marker, true);
                    });

                    $window.google.maps.event.addListener(makerInfo.marker, 'mouseup', function () {
                        var marker = this;
                        api.marker.mouseup(marker, true);
                    });

                    $window.google.maps.event.addListener(makerInfo.marker, 'mousedown', function () {
                        var marker = this;
                        api.marker.mousedown(marker, true);
                    });

                    $window.google.maps.event.addListener(makerInfo.marker, 'mouseover', function () {
                        var marker = this;
                        api.marker.mouseover(marker, true);
                    });

                    $window.google.maps.event.addListener(makerInfo.marker, 'mouseout', function () {
                        var marker = this;
                        api.marker.mouseout(marker, true);
                    });


                    api.drawedMarker['imheremarker'] = makerInfo;
                    api.marker.afterDrawedMarker(makerInfo);
                },
                drawMarker: function (markerOptn, placeResult) {
                    if (api.drawedMarker[placeResult.place_id] === undefined) {

                        api.marker.beforeDrawMarker(markerOptn, placeResult);
                        var markerInfo = { marker: new $window.google.maps.Marker(markerOptn), placeResult: placeResult };

                        if (scope.config.useMarkerClusterer) {
                            markerClusterer.addMarker(markerInfo.marker);
                        }

                        $window.google.maps.event.addListener(markerInfo.marker, 'click', function () {
                            var marker = this;
                            api.marker.click(marker);
                        });

                        $window.google.maps.event.addListener(markerInfo.marker, 'dblclick', function () {
                            var marker = this;
                            api.marker.dbclick(marker);
                        });

                        $window.google.maps.event.addListener(markerInfo.marker, 'mouseup', function () {
                            var marker = this;
                            api.marker.mouseup(marker);
                        });

                        $window.google.maps.event.addListener(markerInfo.marker, 'mousedown', function () {
                            var marker = this;
                            api.marker.mousedown(marker);
                        });

                        $window.google.maps.event.addListener(markerInfo.marker, 'mouseover', function () {
                            var marker = this;
                            api.marker.mouseover(marker);
                        });

                        $window.google.maps.event.addListener(markerInfo.marker, 'mouseout', function () {
                            var marker = this;
                            api.marker.mouseout(marker);
                        });

                        api.drawedMarker[placeResult.place_id] = markerInfo;
                        api.marker.afterDrawedMarker(markerInfo);
                    }
                },
                getPlaceInfo: function(marker) {

                    for (var markerId in api.drawedMarker) {
                        var drawedMarker = api.drawedMarker[markerId].marker;

                        if (drawedMarker.getPosition().lat() === marker.getPosition().lat() && 
                            drawedMarker.getPosition().lng() === marker.getPosition().lng()) {
                            return api.drawedMarker[markerId].placeResult;
                        }
                    }
                },
                isGeoLacalizedMarker: function(marker) {
                    var localizedMarkerInfo = api.drawedMarker['imheremarker'];
                    if (localizedMarkerInfo === undefined) {
                        return false;
                    }
                    
                    var localizedMarker = localizedMarkerInfo.marker;

                    return localizedMarker.getPosition().lat() === marker.getPosition().lat() &&
                           localizedMarker.getPosition().lng() === marker.getPosition().lng();
                },
                drawedMarker: {},
                removeMarker: function (markerId) {
                    var markerInfo = api.drawedMarker[markerId];

                    if (markerInfo !== undefined) {
                        var marker = markerInfo.marker;

                        if (api.directionsDisplay != null && api.directionsDisplay.directions) {
                            var direction = api.directionsDisplay.directions;

                            if (( direction.lc && direction.lc.destination == marker.position ) || (direction.lc && direction.lc.origin == marker.position )) {
                                api.resetDirectionDisplay();
                            }
                            else if ((direction.mc && direction.mc.destination == marker.position) || (direction.mc && direction.mc.origin == marker.position)) {
                                api.resetDirectionDisplay();
                            }
                        }

                        marker.setMap(null);

                        if (scope.config.useMarkerClusterer) {
                            markerClusterer.removeMarker(marker);
                        }

                        delete api.drawedMarker[markerId];
                    }
                },
                marker: {
                    beforeDrawMarker: function (markerOptn, place) { },
                    afterDrawedMarker: function (markerInfo) { },
                    click: function (marker, isGeoLocatedMarker) {
                        api.drawNavigationTo(marker, isGeoLocatedMarker);
                    },
                    dbclick: angular.noop,
                    mouseup: angular.noop,
                    mousedown: angular.noop,
                    mouseover: angular.noop,
                    mouseout: angular.noop,
                    displayStreetViewTo: function(marker, streetViewPov) {
                        if (!marker) { return; }
                        if (!streetViewPov) {
                            streetViewPov = {
                                heading: 265,
                                pitch: 0
                            };
                        }

                        var panorama = map.getStreetView();
                        panorama.setPosition(marker.position);
                        panorama.setVisible(true);

                    },
                    drawNavigationTo: function(marker, isGeoLocatedMarker) {
                        if (isGeoLocatedMarker) {
                            return;
                        }

                        googleMapService.getDirectionTo(marker.position)
                                .then(function (result) {
                                    api.directionsDisplay.setDirections(result);

                                    if (scope.config.directionPanel.enabled) {
                                        api.directionsDisplay.setPanel(
                                            $(element)
                                                .parent()
                                                    .find('.direction-panel')[0]
                                            );

                                        scope.config.directionPanel.directionSet = true;
                                    }
                                });
                    },
                    isMarkerDrawed: function (placeId) {
                        return api.drawedMarker[placeId] !== undefined;
                    }
                },
                hideCustomUi: function () {
                    $(element)
                        .parent()
                            .find('.follow-toggle')
                            .hide();
                    $(element)
                        .parent()
                            .find('.panel-toggle')
                            .hide();
                },
                showCustomUi: function () {
                    $(element)
                        .parent()
                            .find('.follow-toggle')
                            .show();
                    $(element)
                        .parent()
                            .find('.panel-toggle')
                            .show();
                },
                resetDirectionDisplay: function() {
                    if (api.directionsDisplay !== null) {
                        api.directionsDisplay.setMap(null);
                        api.directionsDisplay = new google.maps.DirectionsRenderer();
                        api.directionsDisplay.setMap(map);
                        api.directionsDisplay.setOptions({ suppressMarkers: true });

                        // reset direction panel state 
                        scope.config.directionPanel.directionSet = false;
                    }
                },
                getMap: function () {
                    return map;
                },
                mapInitialized: false,
                directionsDisplay: null
            };

            // Merge scope.api with existing api
            (function () {
                if (scope.api !== undefined) {
                    var markerApi = $.extend({}, api.marker, scope.api && scope.api.marker || {});
                    delete scope.api.marker;
                    api = $.extend({}, api, scope.api);
                    api.marker = markerApi;

                    scope.api = api;
                }
            })();

            // Merge scope.mapOptions with default map options
            var mapOptions = $.extend({}, {
                zoom: 16,
                mapTypeControl: false,
                panControl: false
            }, scope.mapOptions || {});

            // Merge scope.markerClusterOptions with default mc options
            var markerClustererOptions = $.extend({}, {
                gridSize: 50,
                maxZoom: 15
            }, scope.marckerClustererOptions || {});

            var lastLocalizedPosition = null;

            // Create google map canvas div
            $(element)
                .wrap('<div class="map-canvas-container"></div>')
                .append('<div class="map-canvas"></div>')
                .parent()
                    .append('<div class="direction-panel"></div>');

            var map = new $window.google.maps.Map($(element).first('.map-canvas')[0], mapOptions),
                markerClusterer = new $window.MarkerClusterer(map, [], markerClustererOptions);

            // Create custom controls
            (function () {

                // direction panel open | close button
                if (scope.config.directionPanel.enabled) {
                    $(element)
                        .parent()
                            .append('<button type="button" class="btn panel-toggle show-direction-panel-button"><i class="fa fa-road"></i></button>');

                    var btnPanel = $(element)
                        .parent()
                            .find('.panel-toggle')[0];

                    $window.google.maps.event.addDomListener(btnPanel, 'click', function () {
                        scope.$apply(function () {

                            $(btnPanel).toggleClass('active');
                            scope.config.directionPanel.isOpen = !scope.config.directionPanel.isOpen;
                        });
                    });
                    map.controls[$window.google.maps.ControlPosition.TOP_RIGHT].push(btnPanel);
                }

                // map position update button
                $(element)
                     .parent()
                         .append('<button type="button" class="btn follow-toggle you-are-here-button"><i class="fa fa-map-marker"></i></button>');

                var btnFollow = $(element)
                         .parent()
                             .find('.follow-toggle')[0];

                $window.google.maps.event.addDomListener(btnFollow, 'click', function () {
                    scope.$apply(function () {

                        if (lastLocalizedPosition !== null) {
                            map.setCenter(lastLocalizedPosition);
                        }
                    });
                });
                map.controls[$window.google.maps.ControlPosition.RIGHT_TOP].push(btnFollow);

                api.hideCustomUi();
            })();

            // map created, api is ready to be used
            api.mapInitialized = true;

            if (geoLocationService.isGeoLocationSupported()) {
                api.drawMap(map);
                api.directionsDisplay = new google.maps.DirectionsRenderer();
                api.directionsDisplay.setMap(map);
                api.directionsDisplay.setOptions({ suppressMarkers: true });
            }
            else {
                api.showLocationNotAvailable(geoLocationService.GEO_LOCATION_UNSUPPORTED);
            }

            scope.$on('resize', function () {
                $timeout(function () {
                    $window.google.maps.event.trigger(map, "resize");
                }, 250);
            });
           
            scope.$watch('config.directionPanel', function (newVal) {
                if (undefined !== newVal) {

                    if (newVal.enabled && newVal.isOpen && newVal.directionSet) {
                        $(element)
                            .parent()
                                .find('.direction-panel')
                                .first()
                                .slideDown();
                    }
                    else {
                        $(element)
                            .parent()
                                .find('.direction-panel')
                                .first()
                                .slideUp();
                    }
                }
            }, true);
        }
    };
})
;