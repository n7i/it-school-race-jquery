﻿angular.module("budy.widget")

.factory('activitiesModel', function () {
    return {
        researchs: [
            {
                name: 'cinéma',
                gmType: 'movie_theater',
                ticked: true
            },
            {
                name: 'boîte de nuit',
                gmType: 'night_club',
                ticked: true
            },
            {
                name: 'casino',
                gmType: 'casino',
                ticked: true
            },
            {
                name: 'restaurant',
                gmType: 'restaurant',
                ticked: true
            },
            {
                name: 'zoo & aquarium',
                gmType: ['zoo', 'aquarium'],
                ticked: true
            },
            {
                name: 'soin & beauté',
                gmType: [
                    'spa',
                    'hair_care',
                    'beauty_salon'
                ],
                ticked: true
            },
            {
                name: 'culturel',
                gmType: [
                    'museum',
                    'art_gallery'
                ],
                ticked: true
            },
            {
                name: 'bar',
                gmType: [
                    'bar',
                    'cafe'
                ],
                ticked: true
            },
            {
                name: 'stade',
                gmType: 'stadium',
                ticked: true
            },
            {
                name: 'parc d\'attractions',
                gmType: 'amusement_park',
                ticked: true
            },
            {
                name: 'bowling',
                gmType: 'bowling_alley',
                ticked: true
            },
            {
                name: 'magasin',
                gmType: [
                    'home_goods_store',
                    'department_store',
                    'shopping_mall',
                    'shoe_store',
                    'pet_store',
                    'hardware_store',
                    'furniture_store',
                    'florist',
                    'electronics_store',
                    'book_store',
                    'jewelry_store',
                    'liquor_store'
                ],
                ticked: true
            },
            {
                name: 'transport',
                gmType: [
                    'bus_station',
                    'train_station',
                    'taxi_stand'
                ],
                ticked: true
            }
        ],
        selectedGmTypes: [],
        processing: {
            options: {
                stepCount: 12,
                current: 0
            }
        }
    };
})

.controller('ActivitiesCtrl', function ActivitiesController($scope, $window, googleMapService, authService, activitiesModel, markers) {
    var infowindowHolder = {
        window: new $window.google.maps.InfoWindow({}),
        marker: null
    },
    isMapInitialized = false;

    $window.google.maps.event.addListener(infowindowHolder.window, 'domready', function () {
        $('#gm-content-activities')
            .find('#start-navigation-activities')
                .first()
                .on('click', function () {
                    $scope.api.marker.drawNavigationTo(infowindowHolder.marker);
                });

        $('#gm-content-activities')
            .find('#streetview-activties')
                .first()
                .on('click', function () {
                    $scope.api.marker.displayStreetViewTo(infowindowHolder.marker);
                });
    });

    $scope.api = {
        marker: {
            click: function (marker) {
                var title = marker.title,
                    place = $scope.api.getPlaceInfo(marker).vicinity || '',
                    streetPicture = 'https://maps.googleapis.com/maps/api/streetview?size=205x50&location=[B],[K]&key=AIzaSyApY3hhL5LEf92fd4LO_hprFXHM99C0sTw',
                    isLocalizedMarker = $scope.api.isGeoLacalizedMarker(marker),
                    navigateLink = !isLocalizedMarker ? '<a id="start-navigation-activities" href="javascript:void(0);"> Naviguer </a>' : '';

                streetPicture = streetPicture
                                    .replace('[B]', marker.getPosition().lat())
                                    .replace('[K]', marker.getPosition().lng());

                infowindowHolder.marker = marker;
                infowindowHolder.window.close();
                infowindowHolder.window.setContent(
                    '<div id="gm-content-activities" class="gm-iw gm-sm">' +
                         '<div class="gm-title">' + title + '</div>' +
                         '<div class="gm-basicinfo">' +
                            '<div class="gm-addr">' +
                               '' + place +
                            '</div>' +
                            '<div class="gm_photos">' +
                                 navigateLink +  ' <br /><br />' +
                                '<span id="streetview-activties" class="gm-wsv" jsdisplay="!photoImg" jsvalues=".onclick:svClickFn" jstcache="6">' +
                                    '<img jsvalues=".src:svImg" width="204" height="50" jstcache="11" src="' + streetPicture + '">' +
                                    '<label class="gm-sv-label" jstcache="0">Street&nbsp;View</label>' +
                              '</span>' +
                            '</div>' +
                         '</div>' +
                    '</div>'

                );
                infowindowHolder.window.open($scope.api.getMap(), marker);
            },
            beforeDrawMarker: function (markerOptn, place) {
                if (place.types === undefined) {
                    return;
                }

                var placeTypes = place.types.join(' ');

                for (var key in markers) {
                    if (placeTypes.lastIndexOf(key) !== -1) {
                        return markerOptn.icon = markers[key];
                    }
                }
            }
        },
        isPlaceBelongTo: function (place, types) {
            var placesTypes = place.types.join(' ');

            for (var i = 0; i < types.length; ++i) {
                var seekedType = types[i];

                if (placesTypes.lastIndexOf(seekedType) !== -1) {
                    return true;
                }
            }

            return false;
        },
        drawMarkers: function (map, config) {
            if (config !== undefined) {

                // Clean removed places markers by type
                if (config.removedTypes.length) {

                    for (var markerId in $scope.api.drawedMarker) {
                        if (markerId !== 'imheremarker') {

                            var markerInfo = $scope.api.drawedMarker[markerId];
                            if ($scope.api.isPlaceBelongTo(markerInfo.placeResult, config.removedTypes)) {

                                $scope.api.removeMarker(markerId);
                            }
                        }
                    }
                }

                // Seek added places marker by types
                if (config.types.length) {

                    googleMapService.findPlacesByNearWithoutMap(config)
                       .then(function (result) {

                           $.each(result, function (index, placeResult) {

                               var markerOptn = {
                                   map: map,
                                   position: placeResult.geometry.location,
                                   title: placeResult.name,
                                   animation: google.maps.Animation.DROP,
                                   icon: $scope.api.getConfig().marker
                               };

                               $scope.api.drawMarker(markerOptn, placeResult);
                           });
                       },
                       function (reason) {
                           // TODO handle error;
                       });
                }
            }
        }
    };

    $scope.model = activitiesModel;

    $scope.$watch('api.mapInitialized', function (newVal) {
        if (newVal) {
            $scope.user = authService.user();

            if ($scope.user.widgets.activities === undefined) {
                $scope.user.widgets.activities = activitiesModel;
            }

            $scope.model = $scope.user.widgets.activities;
            $scope.model.processing = activitiesModel.processing;
        }
    });

    $scope.$watch('model.researchs', function (newVal, oldVal) {

        if (newVal !== undefined && $scope.api.mapInitialized) {
            var addedTypes = [], removedTypes = [], referential = oldVal;

            $.each(newVal, function (index, elm) {
                var oldElm = referential[index];

                if (elm.ticked !== oldElm.ticked || (elm.ticked && !isMapInitialized)) {
                    if (elm.ticked) {
                        if (elm.gmType instanceof Array) {
                            addedTypes = addedTypes.concat(elm.gmType);
                        }
                        else {
                            addedTypes.push(elm.gmType);
                        }
                    }
                    else {
                        if (elm.gmType instanceof Array) {
                            removedTypes = removedTypes.concat(elm.gmType);
                        }
                        else {
                            removedTypes.push(elm.gmType);
                        }
                    }
                }
            });

            $scope.api.drawMarkers(
                $scope.api.getMap(), {
                    types: addedTypes,
                    removedTypes: removedTypes,
                    minPriceLivel: 0,
                    maxPriceLevel: 4
                }
            );

            isMapInitialized = true;
            $scope.user.widgets.activities = $scope.model;
            $scope.user.$save();
        }
    }, true);
})

.directive('processingButton', function ($timeout) {

    return {
        restrict: 'E',
        templateUrl: "widget/activities/processing-button.tpl.html",
        scope: {
            processing: "="
        },
        link: function (scope, elem, attr) {

            $(elem).one("click", function () {
                
                var self = $(this).find('.pbutton').first(),
                    $loader = self.find('.ppie-loader').first(),
                    sts = [
                      $('<div>', { 'class': 'pright' }).appendTo($loader)[0].style,
                      $('<div>', { 'class': 'pleft' }).appendTo($loader)[0].style
                    ];
                i = 0,
                step = scope.processing.options.stepCount || 12,
                colors = ['transparent', '#96ca12'];


                self.toggleClass("pinvisible");

                (function loop() {
                    sts[+(i > 180)].backgroundImage = 'linear-gradient(' + (i - 90) + 'deg, ' + colors[0] + ' 50%, ' + colors[1] + ' 50%)';

                    if (++i > 360) {
                        i = 0;
                        colors = ["#96ca12", "#96ca12"];
                        self.find("img").first().css("opacity", "0.3");
                        self.addClass("disabled");
                        
                        scope.$apply(function () {
                            scope.processing.processedResult = true;
                        });
                    }
                    else {
                        setTimeout(loop, step);
                    }
                    
                }());
            });
        }
    };
})

;

