﻿(function (undefined) {

    var widgetManager = function () {

        var registered = {};

        function findNextIdx() {
            var top = Number.MIN_VALUE;

            for (var key in registered) {

                var elmIdx = registered[key].idx;
                if (elmIdx > top) {
                    top = elmIdx;
                }
            }
            return top + 1;
        }

        var widgetManager = {
            add: function (name) {

                if (registered[name]) {
                    registered[name].idx = findNextIdx();
                }
            },
            remove: function (name) {

                if (registered[name]) {
                    registered[name].idx = -1;
                }
            },
            register: function (options) {

                if (options.name) {

                    options.style = options.style || 'default';
                    registered[options.name] = options;
                }
            },
            unregisterAll: function() {
                registered = {};
            },
            getWidget: function(name) {
                return registered[name];
            },
            getUsedWidgets: function () {
                var data = [];
                for (var key in registered) {
                    if (registered[key].idx !== -1) {
                        data.push(registered[key]);
                    }
                }
                return data.sort(function (a, b) {
                    return a.idx - b.idx;
                });
            },
            getRegisteredWidgets: function () {
                var data = [];
                for (var key in registered) {
                    data.push(registered[key]);
                }
                return data;
            }
        }; 

        return widgetManager;
    };

    var directive = function (widgetManager) {
        return {
            restrict: 'E',
            templateUrl: "widget/widget.tpl.html",
            scope: {
                options: "="
            },
            link: function (scope, elem, attr) {
                scope.widget = {
                    title: scope.options.title,
                    subtitle: scope.options.subtitle,
                    icon: scope.options.icon,
                    hide: function () {
                        widgetManager.remove(scope.options.name);
                    },
                    size: {
                        wide: function () {
                            scope.options.size = "wide";
                            scope.$broadcast('resize');
                        },
                        normal: function () {
                            scope.options.size = "normal";
                            scope.$broadcast('resize');
                        }
                    }
                };
            }
        };
    };

    angular
        .module("budy.widget")
        .service('widgetManager', widgetManager)
        .directive("widget", directive);
})();