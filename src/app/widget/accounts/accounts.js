﻿angular.module("budy.widget")

.factory('accountModel', function () {
    var accountModel = {
        categories: [
            {
                name: 'alimentation',
                ticked: false
            },
            {
                name: 'allocations',
                ticked: false
            },
            {
                name: 'assurance vie',
                ticked: false
            },
            {
                name: 'crédit immobilier',
                ticked: false
            },
            {
                name: 'impôt',
                ticked: false
            },
            {
                name: 'loyer',
                ticked: false
            },
            {
                name: 'primes',
                ticked: false
            },
            {
                name: 'rentes',
                ticked: false
            },
            {
                name: 'retraites',
                ticked: false
            },
            {
                name: 'salaires',
                ticked: false
            },
            {
                name: 'santé',
                ticked: false
            },
            {
                name: 'sorties',
                ticked: false
            },
            {
                name: 'sport',
                ticked: false
            },
            {
                name: 'vacances',
                ticked: false
            },
            {
                name: 'voitures',
                ticked: false
            },
            {
                name: 'vêtements',
                ticked: false
            }
        ],
        transactions: [
            {
                name: 'débit',
                type: -1
            },
            {
                name: 'crédit',
                type: 1
            }
        ],
        selectedTransaction: null,
        accounts: null
    };

    return accountModel;
})

.controller('AccountsCtrl', function AccountsController($scope, $window, authService, accountModel) {

    $scope.Math = $window.Math;
    $scope.moment = $window.moment;
    $scope.user = authService.user();

    function getSelectedCategories(model) {
        var selected = [];

        for (var category in model.categories) {
            if (model.categories[category].ticked) {
                selected.push(
                    model.categories[category].name);
            }
        }

        return selected;
    }

    $scope.resetFilters = function () {
        $scope.model.selectedTransaction = null;
        for (var key in $scope.model.categories) {
            $scope.model.categories[key].ticked = false;
        }
    };

    $scope.$watch('user', function (nVal) {
        if (nVal) {
            var defaultModel = $.extend({}, accountModel, $scope.user.widgets.accounts || {});
            defaultModel.accounts = $scope.user.accounts;
            defaultModel.filteredAccounts = JSON.parse(JSON.stringify($scope.user.accounts));

            $scope.model = defaultModel;
        }
    });

    $scope.$watch('model', function (nVal, oldVal) {

        function transformAccountHistory(history) {
            // transform transactions raw history likes :
            ///[Months]
            //    [DayOfMonths]
            //        [Transactions]

            var transformedHistoryArray = [],
                currentMonthIdx = -1,
                currentDayOfMonthIdx = -1;
                currentMonth = null,
                currentDayOfMonth = null;

            $.each(history, function (index, elm) {
                var date = moment(elm.date, 'DD/MM/YYYY'),
                    month = date.format('MMMM'),
                    dayOfMonth = date.format('D');

                if (currentMonth !== month) {
                    currentMonth = month;
                    currentDayOfMonth = null;
                    currentDayOfMonthIdx = -1;

                    currentMonthIdx = currentMonthIdx + 1;
                    transformedHistoryArray.push([]);

                }

                if (currentDayOfMonth !== dayOfMonth) {
                    currentDayOfMonth = dayOfMonth;
                    transformedHistoryArray[currentMonthIdx].push([]);
                    currentDayOfMonthIdx = currentDayOfMonthIdx + 1;
                }

                transformedHistoryArray[currentMonthIdx][currentDayOfMonthIdx].push(elm);
            });

            return transformedHistoryArray;
        }


        if (nVal !== undefined) {

            var selectedCategories = getSelectedCategories(nVal),
                selectedCategoriesCount = selectedCategories.length,
                categories = selectedCategories.join(' ');
           
            $.each(nVal.accounts, function (index, account) {
                var history = [];

                $.each(account.history, function (index, element) {

                    if (selectedCategoriesCount && nVal.selectedTransaction) {
                        if (nVal.selectedCategory && element.category && categories.lastIndexOf(element.category) !== -1 &&
                            nVal.selectedTransaction && element.type == nVal.selectedTransaction.type) {

                            history.push(element);
                        }
                    }
                    else if (selectedCategoriesCount && element.category && categories.lastIndexOf(element.category) !== -1) {
                        history.push(element);
                    }
                    else if (nVal.selectedTransaction && element.type == nVal.selectedTransaction.type) {
                        history.push(element);
                    }
                    else if (!selectedCategoriesCount && !nVal.selectedTransaction) {
                        history.push(element);
                    }
                });

                nVal.filteredAccounts[index].history = transformAccountHistory(history);
            });

            $scope.user.widgets.accounts = {
                selectedTransaction: nVal.selectedTransaction,
                categories: nVal.categories
            };

            $scope.user.$save();
        }
    }, true);
})

;

