﻿angular.module("budy.widget")

.factory('markers', function () {
    //http://mapicons.nicolasmollet.com/markers/stores/art-gallery/
    var markers = {
        imhere: 'assets/img/markers/pinother.png',
        movie_theater: 'assets/img/markers/movierental.png',
        night_club: 'assets/img/markers/dancinghall.png',
        casino: 'assets/img/markers/casino-2.png',
        restaurant: 'assets/img/markers/restaurant.png',
        aquarium: 'assets/img/markers/aquarium.png',
        zoo: 'assets/img/markers/zoo.png',
        spa: 'assets/img/markers/massage.png',
        hair_care: 'assets/img/markers/barber.png',
        beauty_salon: 'assets/img/markers/beautysalon.png',
        museum: 'assets/img/markers/art-museum-2.png',
        art_gallery: 'assets/img/markers/artgallery.png.png',
        bar: 'assets/img/markers/bar.png',
        cafe: 'assets/img/markers/coffee.png',
        stadium: 'assets/img/markers/stadium.png',
        amusement_park: 'assets/img/markers/themepark.png',
        bowling_alley: 'assets/img/markers/bowling.png',
        department_store: 'assets/img/markers/departmentstore.png',
        shopping_mall: 'assets/img/markers/supermarket.png',
        shoe_store: 'assets/img/markers/shoes.png',
        pet_store: 'assets/img/markers/cat-2.png',
        hardware_store: 'assets/img/markers/computers.png',
        furniture_store: 'assets/img/markers/homecenter.png',
        florist: 'assets/img/markers/flowers.png',
        electronics_store: 'assets/img/markers/tools.png',
        book_store: 'assets/img/markers/book.png',
        jewelry_store: 'assets/img/markers/jewelry.png',
        liquor_store: 'assets/img/markers/liquor.png',
        bus_station: 'assets/img/markers/bus.png',
        train_station: 'assets/img/markers/train.png',
        taxi_stand: 'assets/img/markers/taxi.png',
        atm: 'assets/img/markers/atm_euro.png',
        bank: 'assets/img/markers/bank_icon.png'
    };

    return markers;
});