angular.module("budy.widget")

.controller('LocalizeCtrl', function LocalizeController($scope, $window, googleMapService, markers) {
    
    var infowindowHolder = {
        window: new $window.google.maps.InfoWindow({}),
        marker: null
    };

    $window.google.maps.event.addListener(infowindowHolder.window, 'domready', function () {
        $('#gm-content-localize')
            .find('#start-navigation-localize')
                .first()
                .on('click', function () {
                    $scope.api.marker.drawNavigationTo(infowindowHolder.marker);
                });

        $('#gm-content-localize')
            .find('#streetview-localize')
                .first()
                .on('click', function () {
                    $scope.api.marker.displayStreetViewTo(infowindowHolder.marker);
                });
    });

    $scope.api = {
        marker: {
            click: function (marker) {
                var title = marker.title,
                    place = $scope.api.getPlaceInfo(marker).vicinity || '',
                    streetPicture = 'https://maps.googleapis.com/maps/api/streetview?size=205x50&location=[B],[K]&key=AIzaSyApY3hhL5LEf92fd4LO_hprFXHM99C0sTw',
                    isLocalizedMarker = $scope.api.isGeoLacalizedMarker(marker),
                    navigateLink = !isLocalizedMarker ? '<a id="start-navigation-localize" href="javascript:void(0);"> Naviguer </a>' : '';

                streetPicture = streetPicture
                                    .replace('[B]', marker.getPosition().lat())
                                    .replace('[K]', marker.getPosition().lng());

                infowindowHolder.marker = marker;
                infowindowHolder.window.close();
                infowindowHolder.window.setContent(
                    '<div id="gm-content-localize" class="gm-iw gm-sm">' +
                         '<div class="gm-title">' + title + '</div>' +
                         '<div class="gm-basicinfo">' +
                            '<div class="gm-addr">' +
                               ''+ place +
                            '</div>' +
                            '<div class="gm_photos">'+
                                navigateLink + '<br /><br />' +
                                '<span id="streetview-localize" class="gm-wsv" jsdisplay="!photoImg" jsvalues=".onclick:svClickFn" jstcache="6">' +
                                    '<img jsvalues=".src:svImg" width="204" height="50" jstcache="11" src="' + streetPicture + '">' +
                                    '<label class="gm-sv-label" jstcache="0">Street&nbsp;View</label>'+
                              '</span>'+
                            '</div>'+
                         '</div>'+
                    '</div>'
                    
                );
                infowindowHolder.window.open($scope.api.getMap(), marker);
            },

            beforeDrawMarker: function (markerOptn, place) {
                if (place.types === undefined) {
                    return;
                }

                if (~place.types.join(' ').lastIndexOf('imhere')) {
                    markerOptn.icon = markers.imhere;
                    
                }
                else
                {
                    if ($scope.model.selectedResearch.gmType === 'atm') {
                        markerOptn.icon = markers.atm;
                    }
                    else {
                        markerOptn.icon = markers.bank;
                    }
                }
            }
        }
    };

    $scope.model = {
        researchs : [
            {
                name: 'distributeur',
                gmType: 'atm'
            },
            {
                name: 'agence',
                gmType: 'bank'
            }
        ]
    };
    $scope.model.selectedResearch = $scope.model.researchs[0];

    $scope.$watch('model.selectedResearch', function (newVal) {
        if (newVal !== undefined && $scope.api.mapInitialized) {
            googleMapService.findPlacesByNearWithoutMap({ types: [newVal.gmType] })
                .then(function (result) {
                    var map = $scope.api.getMap(),
                        config = $scope.api.getConfig();

                    // clean existing markers
                    for (var markerId in $scope.api.drawedMarker) {
                        if (markerId !== 'imheremarker') {

                            $scope.api.removeMarker(markerId);
                        }
                    }

                    // draw new markers
                    $.each(result, function (index, placeResult) {
                        var markerOptn = {
                            map: map,
                            position: placeResult.geometry.location,
                            title: placeResult.name,
                            animation: google.maps.Animation.DROP,
                            icon: config.marker
                        };

                        $scope.api.drawMarker(markerOptn, placeResult);
                    });
                },
                function (reason) {
                    $scope.api.showLocationNotAvailable(reason);
                }
            );
        }
    });
})

;

