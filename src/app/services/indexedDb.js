﻿(function (undefined) {

    var dbService = function ($q) {
        var budyDb = null;
        var version = 2;
        var connection;
        var open = function () {
            if (!indexedDB) { return; }
            var defferConnect = $q.defer();

            var request = indexedDB.open("budy", version);
            var isAvailable = false;
            request.onupgradeneeded = function (e) {
                var db = e.target.result;

                // A versionchange transaction is started automatically.
                //e.target.transaction.onerror = html5rocks.indexedDB.onerror;

                if (db.objectStoreNames.contains("accounts")) {
                    db.deleteObjectStore("accounts");
                }
                db.createObjectStore("accounts", { keyPath: "timeStamp" });

                if (db.objectStoreNames.contains("user")) {
                    db.deleteObjectStore("user");
                }
                db.createObjectStore("user", { keyPath: "timeStamp" });
            };

            request.onsuccess = function (e) {
                defferConnect.resolve(e);
                isAvailable = true;
                budyDb = e.target.result;
                // Do some more stuff in a minute
            };

            request.onerror = function (e) {
                defferConnect.reject(e);
            };
            return defferConnect.promise;
        };
        connection = open();
        return q = {
            opened: function() {
                return isAvailable;
            },
            addData: function (data, storeName) {
                if (storeName === undefined) {
                    storeName = "budy-db";
                }

                var deffered = $q.defer();
                connection.then(function () {
                    var db = budyDb;
                    var trans = db.transaction([storeName], "readwrite");
                    var store = trans.objectStore(storeName);
                    data.timeStamp = new Date().getTime();

                    var request = store.put(data);

                    trans.oncomplete = function (e) {
                        deffered.resolve(e);
                    };

                    request.onerror = function (e) {
                        console.log(e.value);
                        deffered.reject(e);
                    };
                });

                return deffered.promise;
            },
            getData: function (storeName) {
                if (storeName === undefined) {
                    storeName = "budy-db";
                }

                var deffered = $q.defer();
                connection.then(function () {
                    var data = [];
                    var db = budyDb;
                    var trans = db.transaction([storeName], "readwrite");
                    var store = trans.objectStore(storeName);

                    // Get everything in the store;
                    var keyRange = IDBKeyRange.lowerBound(0);
                    var cursorRequest = store.openCursor(keyRange);

                    cursorRequest.onsuccess = function (e) {
                        var result = e.target.result;
                        if (!result) {
                            deffered.resolve(data);
                            return;
                        }

                        data.push(result.value);

                        /* jshint -W024 */
                        result.continue();
                        /* jshint -W024 */
                    };

                    cursorRequest.onerror = function (e) {
                        deffered.reject(e);
                    };
                });

                return deffered.promise;
            },
            account: {
                "user": "Big Buddy",
                "accounts": [{
                    "name": "Compte Courrant",
                    "amount": 1236.62,
                    "history": [{
                        "date": "18/11/2014",
                        "label": "Virement Emis \nWeb S.C.P Bernard Regularisation \nRegularisation Novembre \n18/11/2014 \n",
                        "amount": "100",
                        "type": -1,
                        "category": ""
                    },
                    {
                        "date": "13/11/2014",
                        "label": "Prelevmnt \nPaypal Europe S.A.R.L. Et Cie S.C.A",
                        "amount": "5,18",
                        "type": -1,
                        "category": ""
                    },
                    {
                        "date": "10/11/2014",
                        "label": "Prelevmnt \nS.E.M.I.C. \nCreteil-Habitat-Semic/102651/110814 ",
                        "amount": "452,29",
                        "type": -1,
                        "category": ""
                    },
                    {
                        "date": "09/11/2014",
                        "label": "Virement Emis \nWeb Romain One Tise \nOne Tise \n09/11/2014 \n",
                        "amount": "15",
                        "type": -1,
                        "category": ""
                    },
                    {
                        "date": "07/11/2014",
                        "label": "Invenietis Sarl Vh42401xjesmwy01 \nVirement En Votre Faveur \nSalaire  \n",
                        "amount": "1031,49",
                        "type": 1,
                        "category": ""
                    },
                    {
                        "date": "05/11/2014",
                        "label": "Prelevmnt \nNc Numericable \n",
                        "amount": "27,9",
                        "type": -1,
                        "category": ""
                    }]
                }]
            }
        };
    };

    angular
       .module("budy.service")
       .factory("db", dbService);
})();