﻿angular.module("budy.service")

// Do not care of security issue, it's for prototype only
.factory('mongolabApiKeyInjector', function () {
    var mongolabApiKeyInjector = {
        request: function (config) {

            if (config.url.lastIndexOf('api.mongolab.com') !== -1) {

                config.params = $.extend({}, config.params || {}, {
                        apiKey: '7UhMN4rklbC98Suji8DmT9O9WTFprnep'
                    });
            }

            return config;
        }
    };

    return mongolabApiKeyInjector;
})

.factory('mongolabGETQueryParamParserInterceptor', function () {
    function extractMongolabApiParams(params) {
        var mongolabParams = ['c', 'f', 'fo', 's', 'sk', 'limit', 'apiKey'],
            extractedParams = {};

        for (var key in params) {
            if ($.inArray(key, mongolabParams)  !== -1) {

                extractedParams[key] = params[key];
                delete params[key];
            }
        }

        return extractedParams;
    }

    var mongolabGETQueryParamParserInterceptor = {
        request: function (config) {
            if (config.method === 'GET' && config.url.lastIndexOf('api.mongolab.com') !== -1) {
                
                if (config.params) {
                    var mongolabParams = extractMongolabApiParams(config.params);
                    
                    config.params = $.extend({}, mongolabParams, {
                        q: JSON.stringify(config.params)
                    });
                }
            }

            return config;
        }
    };

    return mongolabGETQueryParamParserInterceptor;
})

.factory('mongolabDefaultUserCreator', function ($q, $injector) {
    var mongolabDefaultUserInterceptor = {
        request: function (config) {

            if (config.method === 'POST' && config.url.lastIndexOf('interceptor://createDefaultUser') !== -1) {

                var deferred = $q.defer(),
                    userInfo = JSON.parse(JSON.stringify(config.params)),
                    configHolder = config,
                    UserRessource = $injector.get('UserRessource');

                UserRessource.$getDefaultUser()
                    .then(function (defaultUser) {
                        
                        var user = $.extend(UserRessource, defaultUser.data, { user: userInfo }),
                            config = $.extend(configHolder, {
                                url: 'https://api.mongolab.com/api/1/databases/budy-db/collections/budy-users?apiKey=7UhMN4rklbC98Suji8DmT9O9WTFprnep',
                                data: user
                            });
                       
                        deferred.resolve(config);
                                    
                    }, 
                    function(reason) {
                        deferred.reject(reason);
                    });
            
                return deferred.promise;
            }

            return config;
        }
    };

    return mongolabDefaultUserInterceptor;
})

.config(function ($httpProvider) {
    $httpProvider.interceptors.push('mongolabApiKeyInjector');
    $httpProvider.interceptors.push('mongolabGETQueryParamParserInterceptor');
    $httpProvider.interceptors.push('mongolabDefaultUserCreator');
})

.factory('UserRessource', function ($resource) {

    var UserRessource = $resource('https://api.mongolab.com/api/1/databases/budy-db/collections/budy-users/:id',
        null,
        {
            findOne: {
                method: 'GET',
                params: {
                    fo: true
                }
            },

            save: {
                method: 'POST'
            },

            getDefaultUser: {
                method: 'GET',
                url: 'assets/account_data.json'
            },

            createDefaultUser: {
                method: 'POST',
                url: 'interceptor://createDefaultUser'
            }
        });

    return new UserRessource();
})
;