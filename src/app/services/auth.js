﻿angular.module("budy.service")

.service('authService', function ($cookies, $location, $q, $injector, UserRessource) {
    var user = null;

    function createUserCookie(user) {

        var now = new Date();
        now.setTime(now.getTime() + (365 * 24 * 60 * 60 * 1000));
        expires = "expires=" + now.toUTCString() + '; ';

        document.cookie = "budy-user" + "=" + JSON.stringify(user) + '; ' + expires + 'path=/';
    }

    var authService = {
        isAuthenticated: function () {
            /* jshint -W018 */
            return !!user === true;
        },

        signInOrCreate: function (username, password, redirect) {
            var deferred = $q.defer();

            if (!username || !password) {
                deferred.reject('User password and username should not be empty !');
                return deferred;
            }

            UserRessource.$findOne({ user: { username: username, password: password } })
                .then(function (result) {
                    if ($.isEmptyObject(result.toJSON())) {

                        UserRessource.$createDefaultUser({ username: username, password: password })
                            .then(function (result) {

                                user = result;
                                createUserCookie(result.user);
                                deferred.resolve(user);

                                if (redirect) {
                                    $location.path(redirect);
                                }
                            },
                            function (reason) {
                                deferred.reject(reason);
                            });
                    }
                    else {

                        user = result;
                        createUserCookie(result.user);
                        deferred.resolve(user);

                        if (redirect) {
                            $location.path(redirect);
                        }
                    }
                },
                function (reason) {
                    deferred.reject(reason);
                });

            return deferred.promise;
        },

        remount: function (redirect) {
            var deferred = $q.defer(),
                userData = $cookies["budy-user"];

            if (null !== userData) {
                try {

                    var userHolder = JSON.parse(userData);
                    authService.signInOrCreate(userHolder.username, userHolder.password)
                        .then(function (result) {
                            deferred.resolve(result);
                            if (redirect) {
                                $location.path(redirect);
                            }
                        },
                        function (reason) {
                            deferred.reject('LOGIN_FAILED');
                        });
                }
                catch (e) {
                    deferred.reject('INVALID_USER_COOKIE');
                }
            }
            else {
                deferred.reject('USER_NOT_AUTHENTICATED');
            }

            return deferred;
        },

        logOff: function (redirect) {
            user = null;
            delete $cookies["budy-user"];
            delete UserRessource._id;
            delete UserRessource.accounts;
            delete UserRessource.user;
            delete UserRessource.widgets;

            if (redirect) {
                $location.path(redirect);
            }
        },
        user: function() {
            return user;
        }
    };

    return authService;

})
;

