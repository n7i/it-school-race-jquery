﻿(function () {
    var config = function ($stateProvider) {
        $stateProvider.state('app.home', {
            url: "/home",
            controller: 'DashboardCtrl',
            templateUrl: 'dashboard/dashboard.tpl.html',
            data: { pageTitle: 'Home' }
        });
    };
    var controller = function ($scope, widgetManager, authService) {
        
        $scope.widgets = widgetManager.getUsedWidgets;
        $scope.registered = widgetManager.getRegisteredWidgets;
        $scope.toggleWidget = function (widget) {
            if (widget.idx !== - 1){
                widgetManager.remove(widget.name);
            }
            else {
                widgetManager.add(widget.name);
            }
        };

        $scope.user = authService.user();

        $scope.$watch('user', function (nVal) {
            if (nVal) {
               
                // Logged user as changed, reset ui
                widgetManager.unregisterAll();

                var widgetConfig = nVal.widgets.config;
                widgetManager.register({
                    size: widgetConfig.activities && widgetConfig.activities.size || 'wide',
                    idx: widgetConfig.activities && widgetConfig.activities.idx || 0,
                    name: "activities",
                    style: "danger",
                    templateUrl: 'widget/activities/activities.tpl.html',
                    title: "Sortir",
                    subtitle: "Des sorties adaptées à vos finances",
                    icon: "activities-icon"
                });
                widgetManager.register({
                    size: widgetConfig.accounts && widgetConfig.accounts.size || 'wide',
                    idx: widgetConfig.accounts && widgetConfig.accounts.idx || 1,
                    name: "accounts",
                    style: "info",
                    templateUrl: 'widget/accounts/accounts.tpl.html',
                    title: "Mes comptes",
                    subtitle: "Vos activités bancaires",
                    icon: "accounts-icon"
                });

                widgetManager.register({
                    size: widgetConfig.localize && widgetConfig.localize.size || 'wide',
                    idx: widgetConfig.localize && widgetConfig.localize.idx || 2,
                    name: "localize",
                    templateUrl: 'widget/localize/localize.tpl.html',
                    title: "Localiser",
                    subtitle: "Trouver un distributeur ou une agence",
                    icon: "localize-icon"
                });
            }
        });

        $scope.$watch($scope.registered, function (nVal, oldVal) {
            if (nVal !== undefined && nVal.length) {

                $.each(nVal, function (index, elm) {
                    $scope
                        .user
                        .widgets
                        .config[elm.name] = {
                            size: elm.size,
                            idx: elm.idx
                        };
                });

                $scope.user.$save();
            }
        }, true);

    };

    angular
        .module('budy.dashboard')
        .config(config)
        .controller('DashboardCtrl', controller);
})();




