﻿(function () {
    function authController($scope, authService) {
       
        $scope.signIn = function () {
            authService.signInOrCreate($scope.username, $scope.password, 'home');
        };

        authService.remount('home');
    }

    angular
        .module("budy.authentication", ["ui.router", "ngCookies"])
        .config(["$stateProvider", function ($stateProvider) {
            $stateProvider
                .state('authentication', {
                    templateUrl: "authentication/layout.tpl.html"
                })
                .state('authentication.index', {
                    url: '/authentication',
                    templateUrl: "authentication/authentication.tpl.html",
                    controller: "AuthController",
                    data: { pageTitle: 'Authentication' }
                })
                .state('authentication.logout', {
                    url: '/logout',
                    data: { pageTitle: 'Authentication' }
                });
        }])
        .controller("AuthController", ["$scope", "authService", authController]);
})();