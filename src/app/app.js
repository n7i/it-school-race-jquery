﻿(function () {
    var config = function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');
        $stateProvider
            .state('app', {
                templateUrl: "layout.tpl.html"
            });
    };

    var ajaxInterceptor = function ($rootScope) {
        var loaderInterceptor = {
            request: function (config) {
                $rootScope.$broadcast('ajaxSend');
                return config;
            },

            response: function(response) {
                $rootScope.$broadcast('ajaxComplete');
                return response;
            }
        };

        return loaderInterceptor;
    };

    var appCtrl = function ($scope, $location, $window, authService) {

        $scope.ajaxInProgress = false;

        $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

            if (toState.name === 'authentication.logout') {
                return authService.logOff('authentication');
            }

            if (!authService.isAuthenticated() && toState.name !== 'authentication.index') {
                return $location.path('/authentication');
            }

            if (angular.isDefined(toState.data.pageTitle)) {
                $scope.pageTitle = toState.data.pageTitle + ' | Budy';
            }
        });

        $scope.$on('ajaxSend', function () {
            $scope.ajaxInProgress = true;
        });

        $scope.$on('ajaxComplete', function () {
            $scope.ajaxInProgress = false;
        });

        //set moment default local globaly
        $window.moment.locale('fr', {
            months: "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
            monthsShort: "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
            weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
            weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
            weekdaysMin: "Di_Lu_Ma_Me_Je_Ve_Sa".split("_")
        });
    };

    angular.module("budy.service", ['ngCookies', 'ngResource']);
    angular.module('budy.widget', ['googleApi', 'multi-select', 'budy.service', 'lineFilters', 'duScroll']);
    angular.module('budy.dashboard', ['ui.router', 'budy.widget']);

    angular
        .module('budy', [
            'budy.authentication',
            'budy.widget',
            'templates-app',
            'templates-common',
            'budy.dashboard',
            'ui.router',
            'secret'
        ])
        .factory('ajaxInterceptor', ajaxInterceptor)
        .config(config)
        .config(function ($httpProvider) {
            $httpProvider.interceptors.push('ajaxInterceptor');
        })
        .controller("AppCtrl", appCtrl);
})();
